from django.db import models
from django.conf import settings

USER_NAME = settings.AUTH_USER_MODEL


# Create your models here.
class Company(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()

    # projects = models.ManyToManyField(
    #     "projects.Project", related_name="company"
    # )
    employees = models.ForeignKey(
        USER_NAME,
        related_name="company",
        default=None,
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.name

from django.urls import path

from companys.views import (
    CompanyListView,
)

urlpatterns = [
    path("", CompanyListView.as_view(), name="list_companys"),
]

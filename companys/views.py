from django.shortcuts import render
from companys.models import Company
from projects.models import Project
from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse

# Create your views here.


class CompanyListView(LoginRequiredMixin, ListView):
    model = Company
    template_name = "company/c_list.html"
    context_object_name = "company_list"

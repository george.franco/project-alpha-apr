from django.db import models
from django.conf import settings

USER_NAME = settings.AUTH_USER_MODEL

# Create your models here.
class Project(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    is_private = models.BooleanField(default=False)
    is_completed = models.BooleanField(default=False)
    members = models.ManyToManyField(USER_NAME, related_name="projects")
    company = models.ForeignKey(
        "companys.Company",
        default=1,
        related_name="projects",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.name
